# 「7.2　バッチ型のデータパイプライン」の手順

## 前提

[Readme](../Readme.md) のリンク元からの続き。

つまり、下記の状態からスタート。

- minioコンテナは上がっている状態
- DevContainerをコンテナ群のネットワーク名へ追加済み
- HostPCのShellのカレントディレクトリは`handson-technologies-powering-big-data/wdpressplus-bigdata/`
- Devcontainerのカレントディレクトリは`handson-technologies-powering-big-data/`

## 手順

以降は本の手順を実行していく。途中、devcontainerからコンテナ群へ接続するときは接続先をlocalhostではなくdocker-compose.ymlのサービス名に変更する必要があるので注意。例えば、次の手順ではendpoint-urlの`http://localhost:9000`を`http://minio:9000`に変更している。

Devcontainer：
```shell
pushd ./wdpressplus-bigdata/

export AWS_ACCESS_KEY_ID=accesskey
export AWS_SECRET_ACCESS_KEY=secretkey

for name in datalake warehouse prefect; do \
aws --endpoint-url http://minio:9000 \
s3api create-bucket --bucket $name; done
```

HostPC：
```shell
docker-compose run --rm metastore /initSchema
docker-compose up -d metastore
```

Devcontainer：
```shell
python scripts/download.py
aws --endpoint-url http://minio:9000 \
s3 cp --recursive ./raw s3://datalake/uscrn/2020-01-01
```

HostPC：
```shell
docker-compose run --rm spark-submit scripts/warehouse.py 2020-01-01
```

Devcontainer：
```shell
aws --endpoint-url http://minio:9000 \
s3 ls --recursive s3://warehouse
```

HostPC：
```shell
docker-compose up -d presto-server
docker-compose run --rm presto-cli
```

presto-cli：
```shell
use hive.default;
show tables;
SELECT wbanno,
    min_by(timestamp, temperature) timestamp_min,
    min(temperature) t_min,
    max_by(timestamp, temperature) timestamp_max,
    max(temperature) t_max
    FROM uscrn
    GROUP by 1
;
SELECT count(*) FROM uscrn
    WHERE timestamp < DATE '2000-01-01';
SELECT count(*) FROM uscrn
    WHERE year < '2000' AND timestamp < DATE '2000-01-01';
SELECT count(*) FROM uscrn;
CREATE SCHEMA IF NOT EXISTS datamart;
DROP TABLE IF EXISTS datamart.uscrn_summary;
CREATE TABLE datamart.uscrn_summary AS
    SELECT date_trunc('day', timestamp) time,
        wbanno,
        avg(temperature) avg_temperature
    FROM uscrn GROUP BY 1, 2
;
exit
```

HostPC：
```shell
docker-compose up -d datamart
docker-compose run --rm python scripts/datamart.py
docker-compose up metabase
```

HostPCのブラウザで`http://localhost:13000`を開き、初期設定する。

|項目|値|
|--|--|
|データベースのタイプ|Presto|
|名前|Presto|
|ホスト|presto-server|
|ポート|8080|
|データベース名|hive|
|ユーザー名|presto|
|パスワード|(空欄)|

Metabaseで遊ぶ。

HostPC：
```shell
docker-compose run --rm python scripts/temperature_features.py
docker-compose run --rm spark-submit scripts/precipitation_features.py 2020-01-01
docker-compose run --rm jupyter-console
```

jupyter-console：
```python
import sqlalchemy
import pandas as pd
uri = "postgresql://datamart:datamart@datamart:5432/datamart"
engine = sqlalchemy.create_engine(uri)
df1 = pd.read_sql_table('temperature_features', con=engine)
df2 = pd.read_sql_table('precipitation_features', con=engine)
df3 = pd.merge(df1, df2, on=['wbanno'])
df3
from sklearn import linear_model
x = df3[['t_min', 't_max']].values
y = df3[['p_avg']].values.flatten()
regr = linear_model.LinearRegression()
regr.fit(x, y)
regr.predict([[-30.8, 23.3], [-32.0, 24.8]])
exit
```

Devcontainerを終了する。

その後、下記コマンドでwdpressplus-bigdataのコンテナ群を終了する。（コンテナ群を先に終了すると、ネットワークを削除できない。この場合、ネットワークを手動削除する必要がある。）

HostPC：
```shell
docker-compose down
```
