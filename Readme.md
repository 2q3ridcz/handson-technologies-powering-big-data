# handson-technologies-powering-big-data

書籍「[［増補改訂］ビッグデータを支える技術 ――ラップトップ1台で学ぶデータ基盤のしくみ](https://books.rakuten.co.jp/rb/16601726/)」の勉強メモ。

## 本の目次

- 第１章　ビッグデータの基礎知識
- 第２章　ビッグデータの探索
- 第３章　ビッグデータの分散処理
- 第４章　ビッグデータの蓄積
- 第５章　ビッグデータのパイプライン
- 第６章　ビッグデータと機械学習
- 第７章　「実践」ビッグデータ分析基盤の構築

## 参考

- [［増補改訂］ビッグデータを支える技術 ――ラップトップ1台で学ぶデータ基盤のしくみ · GitHub](https://github.com/wdpressplus-bigdata)
- [本書について ―改訂にあたって：［増補改訂］ビッグデータを支える技術 ――ラップトップ1台で学ぶデータ基盤のしくみ｜技術評論社](https://gihyo.jp/book/2021/978-4-297-11952-2/content/preface)
- [「［増補改訂］ビッグデータを支える技術」を書きました - Qiita](https://qiita.com/k24d/items/a730cac5704ee72de756)


## メモ

handsonは第7章のみ実施。章立てに沿って、気になったとこにコメント。

### 第７章　「実践」ビッグデータ分析基盤の構築

#### 7.1　ノートブックとアドホック分析

(「7.1　ノートブックとアドホック分析」完了時点の成果物は [v0.0.1] 参照。)

multipassで構築した仮想サーバ上で作業する節。

本ではmultipassを使用しているが、今回はインストールしない。（PCに20GBの空き容量がないため断念…）Docker（VSCode + devcontainer）でできるとこまでやる。先読みして確認したら、Prefectのところで躓きそうな予感。その時考える。

Docker imageは、手元にある`python:3.7.16`を使用する。`cat /etc/os-release`コマンドで確認したところ、OSはDebian GNU/Linux 11 (bullseye)。このOSではJava8をインストールできなかったため、Java11にした。本では Ubuntu 20.04.1 LTS、Python 3.8.5、Java8。

また、本に書いてある`apt install`、`pip install`系は実施していない。処理が動かなかったときに都度installする。installしたものは[requirements.txt](./requirements.txt)に記載していく。

本ではJupyterLabを使用するが、VSCodeを使用する。

などなど色々違いはあったが、「7-1.ipynb」を無事に実行できた。

作業の流れは下記のとおり。

1. 当リポジトリ（handson-technologies-powering-big-data）を`git clone`する
2. 当リポジトリのdevcontainerを起動する。
3. wdpressplus-bigdataを`git clone`する
4. 「7-1.ipynb」を実施する
5. devcontainerを終了する

#### 7.2　バッチ型のデータパイプライン

(「7.2　バッチ型のデータパイプライン」完了時点の成果物は [v0.0.2] 参照。)

[wdpressplus-bigdata]のコンテナ群を起動しながら作業する節。

コンテナ群の外でdockerやAWS CLIで操作する必要がある。HostPCにAWS CLIをインストールしたくない、でもdevcontainerではdocker操作できない。そのため、docker操作をHostPCから、AWS CLI操作をdevcontainerからやる。

```mermaid
flowchart LR
subgraph "HostPC"
hostpc["HostPCのShell"]
wdpress["wdpressplus-bigdataのコンテナ群"]
devcontainer["Devcontainer"]
hostpc -- "Dockerコマンドでの起動、操作" --> wdpress
hostpc -- "起動" --> devcontainer
devcontainer -- "AWS CLIでの操作" --> wdpress
end
```

Devcontainerとコンテナ群で通信できるようにするため、devcontainerをコンテナ群のネットワークに参加させる。そのために以下の流れで作業する。

1. 当リポジトリ（handson-technologies-powering-big-data）を`git clone`する
2. 当リポジトリのdevcontainerを起動する。
3. wdpressplus-bigdataを`git clone`する
4. wdpressplus-bigdataのコンテナ群を起動する（全部でなくとも、1つでも起動すればOK）。この時、コンテナ群のネットワークが作成される
5. コンテナ群のネットワークにdevcontainerを追加する
6. 本の手順を実施する
7. devcontainerを終了する
8. wdpressplus-bigdataのコンテナ群を終了する。devcontainerより後で終了すれば、同時にネットワークも削除される

流れの1、2は終わっている前提で、次の手順で3、4を実施する。`handson-technologies-powering-big-data`のルートフォルダで`git clone`し、以降HostPCでは`handson-technologies-powering-big-data/wdpressplus-bigdata/`で作業する。

HostPC：
```shell
git clone https://github.com/wdpressplus-bigdata/wdpressplus-bigdata.git
pushd ./wdpressplus-bigdata/
docker-compose up -d minio
```

次の手順では`docker network ls`でコンテナ群のネットワーク名を、`docker ps`でdevcontainerのコンテナ名を確認する。そしてこれらを用いてdevcontainerをコンテナ群のネットワークに追加する。ネットワーク名は`wdpressplus-bigdata_default`と想定。コンテナ名はランダム文字列。

HostPC：
```shell
docker network ls
docker ps
docker network connect <ネットワーク名> <コンテナ名>
```

以降は本の手順を実行できた（[7-2-operations.md](./docs/7-2-operations.md)）。途中、devcontainerからコンテナ群へ接続するときは接続先をlocalhostではなくdocker-compose.ymlのサービス名に変更する必要があるので注意。

#### 7.3　ワークフロー管理ツールによる自動化

(未)

#### 7.4　まとめ

(未)

[wdpressplus-bigdata]: https://github.com/wdpressplus-bigdata/wdpressplus-bigdata
[v0.0.1]: https://gitlab.com/2q3ridcz/handson-technologies-powering-big-data/-/tree/v0.0.1
[v0.0.2]: https://gitlab.com/2q3ridcz/handson-technologies-powering-big-data/-/tree/v0.0.2
