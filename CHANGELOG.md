# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - yyyy-mm-dd

## [0.0.2] - 2023-05-21
### Changed
- 「7.2　バッチ型のデータパイプライン」を完了

## [0.0.1] - 2023-05-21
### Added
- 「7.1　ノートブックとアドホック分析」を完了

[Unreleased]: https://gitlab.com/2q3ridcz/handson-technologies-powering-big-data/-/compare/v0.0.2...main
[0.0.2]: https://gitlab.com/2q3ridcz/handson-technologies-powering-big-data/-/compare/v0.0.2...v0.0.1
[0.0.1]: https://gitlab.com/2q3ridcz/handson-technologies-powering-big-data/-/tree/v0.0.1
